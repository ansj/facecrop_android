package gmsoft.com.facesticker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.File;
import java.util.List;

/**
 * Created by sjartuni on 22/12/16.
 */

public class FaceAdapter extends BaseAdapter {

    private Context mContext;
    private GMSDBHelper db;
    private List<FaceInfo> faceList;

    // Constructor
    public FaceAdapter(Context c) {
        mContext = c;
        this.db = new GMSDBHelper(c);
        this.faceList = db.getAllFaces();
    }

    public int getCount() {
        return this.faceList.size();
    }

    public void updateContent()
    {
        this.faceList = db.getAllFaces();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(1, 1, 1, 1);
        }
        else
        {
            imageView = (ImageView) convertView;
        }

        imageView.setImageBitmap(getImageFromFile(position));
        return imageView;
    }

    private Bitmap getImageFromFile(int position)
    {
        FaceInfo fi = faceList.get(position);
        long theID = fi.id;

        String fileName = String.valueOf(theID) + ".png";
        String filePath = this.mContext.getFilesDir() + fileName;

        File imgFile = new  File(filePath);

        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            return myBitmap;

        }
        return null;
    }

}
