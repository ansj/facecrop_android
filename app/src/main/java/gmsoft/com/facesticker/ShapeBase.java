package gmsoft.com.facesticker;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by anantasjartuni on 19/12/16.
 */

abstract public class ShapeBase {

    public Bitmap cropFace(Bitmap inBitmap, RectF faceRect) {

        Bitmap croppedBmp = Bitmap.createBitmap(inBitmap, (int) faceRect.left, (int) faceRect.top, (int) faceRect.width(), (int) faceRect.height());
        return  croppedBmp;

    }

    public void saveBitmap(Bitmap bitmap, String filepath)
    {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filepath);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    abstract  Bitmap getBitmap(Bitmap inBitmap, RectF frame);
    abstract Path DrawShape(Canvas canvas, RectF frame);
}
