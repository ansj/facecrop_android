package gmsoft.com.facesticker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import static android.content.ContentValues.TAG;

/**
 * Created by anantasjartuni on 17/12/16.
 */

public class GMSCropFace {

    private SparseArray<Face> mFaces;

    Context context;
    Bitmap inImage;
    private GMSDBHelper db;
    String imageURI;

    public GMSCropFace(Context contex, String imgURI)
    {
        this.imageURI = imgURI;
        this.context = contex;
        this.db = new GMSDBHelper(contex);
    }


    public  boolean detectFace(Bitmap image)
    {
        Boolean retVal = false;
        this.inImage = image;

        FaceDetector detector = new FaceDetector.Builder( this.context )
                .setTrackingEnabled(false)
                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
                .setMode(FaceDetector.ACCURATE_MODE)
                .build();

        if (!detector.isOperational()) {
            //Handle contingency
        } else {
            Frame frame = new Frame.Builder().setBitmap(image).build();
            mFaces = detector.detect(frame);
            detector.release();

            if (mFaces.size() > 0) {
                retVal  = true;
            }
            cropFaceAndSaveInfoToDB();
        }
        return  retVal;
    }

    public  void cropFaceAndSaveInfoToDB()
    {
        for (int i = 0; i < mFaces.size(); i++)
        {
            Face f = mFaces.valueAt(i);
            float left = f.getPosition().x;
            float top = f.getPosition().y;
            float width = f.getWidth();
            float height = f.getHeight();

            if (width > height)
            {
                float rest = (width - height) / 2;
                top -= rest;

                height = width;
            }
            else
            {
                float rest = (height - width) / 2;
                left -= rest;
                width = height;
            }

            RectF frame = new RectF(left, top, left + width, top + height);

            // this is for testing only
            ShapeCircle cir = new ShapeCircle();

            //Bitmap faceImg = cir.cropFace(this.inImage, frame);
            
            Bitmap shapeImg = cir.getBitmap(this.inImage, frame);

            Log.d(TAG, "cropFaceAndSaveInfoToDB: resutl");

            Rect rect = new Rect();
            frame.roundOut(rect);

            long theID = db.addImageRect(this.imageURI, "ShapeCircle", rect);

            if (theID >= 0)
            {
                String fileName = String.valueOf(theID) + ".png";
                String filePath = context.getFilesDir() + fileName;
                cir.saveBitmap(shapeImg, filePath);
            }

        }
    }
}
