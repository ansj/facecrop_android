package gmsoft.com.facesticker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sjartuni on 18/12/16.
 */


public class GMSDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "facesticker.db";
    public GMSDBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table images " +
                        "(id integer primary key, imgURI text )"
        );

        db.execSQL(
                "create table faces " +
                        "(id integer primary key, imgURI text, class text, rect text )"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean addImage(String uri) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("imgURI", uri);
        db.insert("images", null, contentValues);
        return true;
    }

    public long addImageRect(String uri, String objClass, Rect rect) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("imgURI", uri);
        contentValues.put("class", objClass);
        contentValues.put("rect", rect.flattenToString());
        return db.insert("faces", null, contentValues);
    }

    public long FaceCount()
    {
        String countQuery = "SELECT  * FROM faces";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    // Getting All Contacts
    public List<FaceInfo> getAllFaces() {
        List<FaceInfo> faceList = new ArrayList<FaceInfo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM faces";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                FaceInfo face = new FaceInfo();
                face.id =  Integer.parseInt(cursor.getString(0));
                face.URI = cursor.getString(1);
                face.shape = cursor.getString(2);
                // Adding contact to list
                faceList.add(face);
            } while (cursor.moveToNext());
        }

        // return contact list
        return faceList;
    }
}
