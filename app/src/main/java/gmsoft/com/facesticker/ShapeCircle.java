package gmsoft.com.facesticker;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;


/**
 * Created by anantasjartuni on 19/12/16.
 */

public class ShapeCircle extends  ShapeBase {

    public Bitmap getBitmap(Bitmap inBitmap, RectF frame) {

        Bitmap bmpFace = cropFace(inBitmap, frame);

        int targetWidth = (int) frame.width();
        int targetHeight = (int) frame.height();
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(targetBitmap);

        frame = new RectF(0,0,targetWidth, targetHeight);

        // below is the code from paint code remember the path to use to clip named ++++ facePath +++++++
        // --------------------------------------------------

        Path facePath = DrawShape(canvas, frame);

        // -------------------------------------------------------------------------------------------
        // this is part of the end of the code
        canvas.clipPath(facePath);
        canvas.drawBitmap(bmpFace,
                new Rect(0, 0, bmpFace.getWidth(),
                        bmpFace.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), new Paint(Paint.FILTER_BITMAP_FLAG));

        return targetBitmap;

    }

    // return the shape to fill the face
    Path DrawShape(Canvas canvas, RectF frame)
    {
        // General Declarations
        Paint paint;

        // Rectangle
        RectF rectangleRect = new RectF(
                frame.left + (float) Math.floor(frame.width() * 0.05833f + 0.5f),
                frame.top + (float) Math.floor(frame.height() * 0.05f + 0.5f),
                frame.left + (float) Math.floor(frame.width() * 0.95833f + 0.5f),
                frame.top + (float) Math.floor(frame.height() * 0.95833f + 0.5f));
        Path rectanglePath = new Path();
        rectanglePath.addRect(rectangleRect, Path.Direction.CW);

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.GREEN);
        canvas.drawPath(rectanglePath, paint);

        // face
        RectF faceRect = new RectF(
                frame.left + (float) Math.floor(frame.width() * 0.09167f + 0.5f),
                frame.top + (float) Math.floor(frame.height() * 0.09167f + 0.5f),
                frame.left + (float) Math.floor(frame.width() * 0.91667f + 0.5f),
                frame.top + (float) Math.floor(frame.height() * 0.91667f + 0.5f));
        Path facePath = new Path();
        facePath.addOval(faceRect, Path.Direction.CW);

        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.GRAY);
        canvas.drawPath(facePath, paint);

        return facePath;
    }
}
