package gmsoft.com.facesticker;

import android.graphics.RectF;

/**
 * Created by sjartuni on 22/12/16.
 */

public class FaceInfo {
    public String URI;
    public RectF frame;
    public long id;
    public String shape;
}
