package gmsoft.com.facesticker;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * Created by anantasjartuni on 21/12/16.
 */

public class ShapeLoveStar extends ShapeBase {

    public Bitmap getBitmap(Bitmap inBitmap, RectF frame) {

        Bitmap bmpFace = cropFace(inBitmap, frame);

        int targetWidth = (int) frame.width();
        int targetHeight = (int) frame.height();
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(targetBitmap);

        frame = new RectF(0,0,targetWidth, targetHeight);

        // below is the code from paint code remember the path to use to clip named ++++ facePath +++++++
        // --------------------------------------------------

        Path facePath = DrawShape(canvas, frame);

        // -------------------------------------------------------------------------------------------
        // this is part of the end of the code
        canvas.clipPath(facePath);
        canvas.drawBitmap(bmpFace,
                new Rect(0, 0, bmpFace.getWidth(),
                        bmpFace.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), new Paint(Paint.FILTER_BITMAP_FLAG));

        return targetBitmap;

    }


    Path DrawShape(Canvas canvas, RectF frame) {
        // General Declarations
        Paint paint;

        // Local Colors
        int strokeColor = Color.argb(255, 151, 151, 151);

        // Local Images
        //Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.image);

        // Rectangle
        RectF rectangleRect = new RectF(
                frame.left,
                frame.top,
                frame.left + (float) Math.floor(frame.width() + 0.5f),
                frame.top + (float) Math.floor(frame.height() + 0.5f));
        Path rectanglePath = new Path();
        rectanglePath.addRect(rectangleRect, Path.Direction.CW);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // Warning: Fill by raster image is not supported yet. It will be added very soon.

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(1f);
        paint.setStrokeMiter(10f);
        canvas.save();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(strokeColor);
        canvas.drawPath(rectanglePath, paint);
        canvas.restore();

        // face
        RectF faceRect = new RectF(
                frame.left + 31.54f,
                frame.top + 60.5f,
                frame.left + 291.17f,
                frame.top + 250.89f);
        Path facePath = new Path();
        facePath.moveTo(frame.left + frame.width() * 0.90837f, frame.top + frame.height() * 0.39938f);
        facePath.cubicTo(frame.left + frame.width() * 0.92515f, frame.top + frame.height() * 0.28304f, frame.left + frame.width() * 0.8021f, frame.top + frame.height() * 0.19102f, frame.left + frame.width() * 0.69689f, frame.top + frame.height() * 0.19102f);
        facePath.cubicTo(frame.left + frame.width() * 0.5968f, frame.top + frame.height() * 0.19102f, frame.left + frame.width() * 0.54049f, frame.top + frame.height() * 0.28948f, frame.left + frame.width() * 0.50434f, frame.top + frame.height() * 0.33768f);
        facePath.cubicTo(frame.left + frame.width() * 0.4779f, frame.top + frame.height() * 0.28885f, frame.left + frame.width() * 0.38541f, frame.top + frame.height() * 0.18906f, frame.left + frame.width() * 0.3242f, frame.top + frame.height() * 0.18906f);
        facePath.cubicTo(frame.left + frame.width() * 0.16966f, frame.top + frame.height() * 0.18906f, frame.left + frame.width() * 0.09723f, frame.top + frame.height() * 0.30465f, frame.left + frame.width() * 0.09894f, frame.top + frame.height() * 0.39937f);
        facePath.cubicTo(frame.left + frame.width() * 0.09809f, frame.top + frame.height() * 0.40355f, frame.left + frame.width() * 0.09771f, frame.top + frame.height() * 0.42557f, frame.left + frame.width() * 0.10836f, frame.top + frame.height() * 0.46147f);
        facePath.cubicTo(frame.left + frame.width() * 0.1237f, frame.top + frame.height() * 0.51326f, frame.left + frame.width() * 0.19039f, frame.top + frame.height() * 0.56037f, frame.left + frame.width() * 0.24206f, frame.top + frame.height() * 0.59767f);
        facePath.lineTo(frame.left + frame.width() * 0.49987f, frame.top + frame.height() * 0.78405f);
        facePath.lineTo(frame.left + frame.width() * 0.76211f, frame.top + frame.height() * 0.59768f);
        facePath.cubicTo(frame.left + frame.width() * 0.80718f, frame.top + frame.height() * 0.55563f, frame.left + frame.width() * 0.87911f, frame.top + frame.height() * 0.51561f, frame.left + frame.width() * 0.89582f, frame.top + frame.height() * 0.46147f);
        facePath.cubicTo(frame.left + frame.width() * 0.90688f, frame.top + frame.height() * 0.42564f, frame.left + frame.width() * 0.90921f, frame.top + frame.height() * 0.40356f, frame.left + frame.width() * 0.90837f, frame.top + frame.height() * 0.39938f);
        facePath.close();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(0.5f);
        paint.setStrokeMiter(10f);
        canvas.save();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        canvas.drawPath(facePath, paint);
        canvas.restore();

        return facePath;
    }


}
